import { Component } from '@angular/core';
import { Messages } from 'primeng/messages';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'encuesta';
}
