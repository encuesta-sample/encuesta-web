import { Component, OnInit } from '@angular/core';
import { EncuestaService } from '../services/encuesta.service';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit {

  basicData: any;
  colors=["#42A5F5","#FFA726","#00ff00", "#ffd700","#d874e1"];

  constructor(private encuestaService:EncuestaService) { 
    this.basicData = {
      labels: [''],//'January', 'February', 'March', 'April', 'May', 'June', 'July'],
      datasets: []/*
          {
              label: 'My First dataset',
              backgroundColor: '#42A5F5',
              data: [65]
          },
          {
              label: 'My Second dataset',
              backgroundColor: '#FFA726',
              data: [28]
          }
      ]*/
  };
  }

  ngOnInit(): void {

    this.encuestaService.result().subscribe(listaResult =>{
      
    this.basicData = {
      labels: [''],
      datasets: []
  };

      listaResult.forEach(estilo=>{
        let ds:any={};
        ds.label=estilo.esmuDescripcion;
        ds.backgroundColor=this.colors[this.basicData.datasets.lenght];
        ds.data=[];
        ds.data.push(estilo.cant);
        this.basicData.datasets.push(ds);
        //alert(JSON.stringify(this.basicData));
      })
    }, error=>{
      
    });
  }

}
