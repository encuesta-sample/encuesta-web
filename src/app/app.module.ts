import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ButtonModule} from 'primeng/button';
import {CardModule} from 'primeng/card';
import {ChartModule} from 'primeng/chart';
import {DropdownModule} from 'primeng/dropdown';
import { EncuestaService } from './services/encuesta.service';
import { EstiloMusicalService } from './services/estilomusical.service';
import {FormsModule} from "@angular/forms" ;
import { HttpClientModule } from '@angular/common/http';
import {MessagesModule} from 'primeng/messages';
import {MessageModule} from 'primeng/message';
import {MessageService} from 'primeng/api';
import { VotarComponent } from './votar/votar.component';
import { ResultComponent } from './result/result.component';

@NgModule({
  declarations: [
    AppComponent,
    VotarComponent,
    ResultComponent
  ],
  imports: [
    AppRoutingModule,
    ButtonModule,
    BrowserAnimationsModule,
    BrowserModule,
    CardModule,
    ChartModule,
    DropdownModule,
    FormsModule,
    HttpClientModule,
    MessagesModule,
    MessageModule
  ],
  providers: [
    EncuestaService,
    EstiloMusicalService,
    MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
