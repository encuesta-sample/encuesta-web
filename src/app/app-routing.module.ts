import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResultComponent } from './result/result.component';
import { VotarComponent } from './votar/votar.component';

const routes: Routes = [
  {path:'result', component: ResultComponent},
  {path:'votar', component: VotarComponent},
  {path:'', redirectTo: "votar", pathMatch:"full"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
