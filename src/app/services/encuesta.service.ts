import { Injectable } from "@angular/core";
import { HttpClient,HttpParams } from '@angular/common/http';
import { Observable } from "rxjs";

import { ConfigProps } from '../constantes/ConfigProps';
import { EstiloMusicalDTO } from "../dto/EstiloMusicalDTO";
import { EncuestaDTO } from "../dto/EncuestaDTO";
import { EncuestaResultDTO } from "../dto/EncuestaResultDTO";

@Injectable()
export class EncuestaService{
    constructor(private http: HttpClient){}

    create(dto: EncuestaDTO):Observable<void>{
        return this.http.post<void>(ConfigProps.urlRestServices+"encuesta/create",dto);
    }
    result():Observable<EncuestaResultDTO[]>{
        return this.http.get<EncuestaResultDTO[]>(ConfigProps.urlRestServices+"encuesta/result");
    }
}