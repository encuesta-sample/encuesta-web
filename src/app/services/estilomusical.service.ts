import { Injectable } from "@angular/core";
import { HttpClient,HttpParams } from '@angular/common/http';
import { Observable } from "rxjs";

import { ConfigProps } from '../constantes/ConfigProps';
import { EstiloMusicalDTO } from "../dto/EstiloMusicalDTO";

@Injectable()
export class EstiloMusicalService{
    constructor(private http: HttpClient){}

    list():Observable<EstiloMusicalDTO[]>{
        return this.http.get<EstiloMusicalDTO[]>(ConfigProps.urlRestServices+"estilomusical/list");
    }
}