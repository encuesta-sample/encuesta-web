export class EncuestaDTO{
    estiloMusicalCodigo:number;
    email: string;

    constructor(estiloMusicalCodigo: number, email: string) {
        this.estiloMusicalCodigo = estiloMusicalCodigo;
        this.email = email;
    }
}