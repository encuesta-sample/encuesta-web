export class EstiloMusicalDTO{
    codigo:number;
    descripcion: string;

    constructor(codigo: number, descripcion: string) {
        this.codigo = codigo;
        this.descripcion = descripcion;
    }
}