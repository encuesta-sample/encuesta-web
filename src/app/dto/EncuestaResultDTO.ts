export class EncuestaResultDTO{
    esmuCodigo:number;
    esmuDescripcion: string;
    cant:number;

    constructor(esmuCodigo: number, esmuDescripcion: string,cant: number) {
        this.esmuCodigo = esmuCodigo;
        this.esmuDescripcion = esmuDescripcion;
        this.cant = cant;
    }

}