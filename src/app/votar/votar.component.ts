import { Component, OnInit } from '@angular/core';
import { EncuestaService } from '../services/encuesta.service';
import { EstiloMusicalService } from '../services/estilomusical.service';
import {MessageService} from 'primeng/api';
import { EstiloMusicalDTO } from '../dto/EstiloMusicalDTO';
import { EncuestaDTO } from '../dto/EncuestaDTO';
import { Router } from '@angular/router';

@Component({
  selector: 'app-votar',
  templateUrl: './votar.component.html',
  styleUrls: ['./votar.component.css']
})
export class VotarComponent implements OnInit {
  listaEstiloMusical:EstiloMusicalDTO[]=[];
  estiloMusical:number=0;
  email:string="";

  constructor(private estiloMusicalService:EstiloMusicalService, private encuestaService:EncuestaService, private messageService: MessageService,
    private router:Router) { 
    
  }

  ngOnInit(): void {
    this.estiloMusicalService.list().subscribe(listaEstiloMusical=>{
      this.listaEstiloMusical=listaEstiloMusical;
    }, error=>{
      this.messageService.add({severity:'error', summary:'Ha ocurrido un error', detail:JSON.stringify(error)});
      
    });
  }

  clickSubmit():void{
    this.encuestaService.create(new EncuestaDTO(this.estiloMusical,this.email)).subscribe();
    
    this.router.navigate(["result"]);
  }
}
