# Encuesta

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.0.7.

Para una mejor visualización utilizar la herramientasd de desarrollo del navegador F12 y seleccionar vista responsive mobile vertical


## Configuración de ambiente
Para las configuraciones de ambiente se utilizan variables de entorno que son parametrizadas a los contenedores docker.
Este archivo de configuiración es un archivo plano no compilado por angular, ubicado en `src/assets/env.js`. Además este archivo es cargado en `index.html`y los valores de estas variables son traspasados al archivo `src/app/constantes/ConfigProps.ts`.
Para lograr esta hazaña al momento de lavantar la aplicación `nginx` se utiliza el comando `envsubst` especificado en el `Dockerfile`
## Procedimiento de versionamiento
- Actualizar (pull)
- Bloquear archivo
- Hacer modificaciones
- Actualizar
- Commit
- Push
- Desbloquear archivo

## comandos git
- descargar el repositorio					`git clone https://gitlab.com/encuesta-sample/encuesta-web.git`
- cambiarse de branch							`git checkout -b develop`
- actualizar desde gitlab						`git pull origin develop`
- subir cambios a gitlab						`git push -u origin develop`
- bloquear archivo `git lfs lock path/to/file.png`
- desbloquear archivo `git lfs unlock path/to/file.png`
- desbloquear archivo `git lfs unlock --id=123`
- ver archivos bloqueados `git lfs locks`

## Docker
### Creación Imagen docker angular web

#### Opción 2: Local
- Detener el contenedor (si es que se está ejecutando) `docker container stop encuesta-web`
- Eliminar el contenedor (si es que existe previamente) `docker container rm encuesta-web`
- Eliminar la imagen (Si es que existe previamente) `docker rmi -f encuesta/web`
- Abrir una consola dentro del proyecto, donde se ubica el archivo Dockerfile
- Construir Imagen `docker build -t encuesta/web ./`
- Exportación imagen (Solo si es que se quiere subir la imagen a servidor sin integración continua) `docker save encuesta/web > encuesta-web.tar`

### Importación imagen docker Spring boot (Solo si es que no se ha construido localmente)

#### Opción 2: Local
- Abrir una terminal en la ruta donde se encuentre la imagen de docker `encuesta-web`
- Importación imagen `docker load -i encuesta-web.tar`

### Carga Inicial 
#### Desde Gitlab
##### Producción
##### Local desarrollo
- UNIX    `docker run -p 4200:80 -e URL_REST_SERVICES="http://localhost:8080" -it --name encuesta-restful encuesta/restful`

## Carga normal ( omitir si el docker esta ejecutandose)
- Arrancar `docker container start encuesta-web`
- Acoplar ventana `docker attach encuesta-web` Para ver log en tiempo real

## Check vitalidad del servicio

`http://localhost:4200`

## Links de interés
- [Editor de este archivo](https://remarkableapp.github.io/)
- [Modificar este archivo](https://docs.gitlab.com/ee/user/markdown.html)


## TO DO
*	Aplicar Validator para email
*	Aplicar Tests
*	Aplicar respuesta desde los microservicios con codigo y descripción
*	Mostrar mensajes del resiultado de la operación desde el microservicio
*	Aplicar blockui cada vez que se produce una llamda a un microservicio o se está cargando un elemento
*	Integración continua con repositorios de imagenes Docker y orquestador de contenedores y balanceo como kubernete
*	Integrar variables de entorno para configurar los ambientes
*	Integración y revisión de código estático como SonarQuebe
*	Pasar querys estáticas desde clases Repository de Spring data persistence a procedimientos almacenados
 
## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/encuesta-sample/encuesta-web.git
git branch -M main
git push -uf origin main
```


Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
