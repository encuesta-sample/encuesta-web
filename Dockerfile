
#stage 1
FROM node:latest as node
WORKDIR /app
COPY . .
RUN npm install
RUN npm run build --prod
#RUN tail /app/dist/encuesta/assets/env.js
#stage 2
FROM nginx:alpine
COPY --from=node /app/dist/encuesta /usr/share/nginx/html
#RUN tail /usr/share/nginx/html/assets/env.js

# When the container starts, replace the env.js with values from environment variables
CMD ["/bin/sh",  "-c",  "envsubst < /usr/share/nginx/html/assets/env.in.js > /usr/share/nginx/html/assets/env.js && exec nginx -g 'daemon off;'"]
